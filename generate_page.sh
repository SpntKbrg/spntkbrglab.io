#!/usr/bin/env bash

# * Shell config
set -euo pipefail
IFS=$'\n\t'

PAGE_BEGIN="template/page_begin.html"
PAGE_END="template/page_end.html"
PAGE_DATA="template/data.org"
OUT_HTML="public/index.html"

closing='false'

cat "$PAGE_BEGIN" > "$OUT_HTML"
while read -r data ; do
  if [[ ${data:0:1} == '*' ]] ; then
    if [[ "$closing" == 'true' ]] ; then
      echo '  </div>' >> "$OUT_HTML"
    fi
    closing='true'
    echo '    <div class="row col-sm-12">' >> "$OUT_HTML"
    echo '      <h3>' "$(echo "$data" | cut -f2 -d'*' | tr -s ' ')" '</h3>' >> "$OUT_HTML"
    echo '    </div>' >> "$OUT_HTML"
    echo '    <div class="row cols-sm-12" style="justify-content: center; margin: auto">' >> "$OUT_HTML"
  elif [[ ${data:0:1} == '|' ]] ; then
    echo '      <div class="card small" style="height: 360px">' >> "$OUT_HTML"
    echo '        <div class="container">' >> "$OUT_HTML"
    echo '          <a href="'"$(echo "$data" | cut -f5 -d'|' | tr -s ' ')"'">' >> "$OUT_HTML"
    echo '            <img style="margin: auto; height: 160px" src="'"$(echo "$data" | cut -f4 -d'|' | tr -s ' ')"'" class="section media">' >> "$OUT_HTML"
    echo '          </a>' >> "$OUT_HTML"
    echo '        </div>' >> "$OUT_HTML"
    echo '        <div class="section" style="height: 200px">' >> "$OUT_HTML"
    echo '          <a class="button hover" style="width: auto" href="'"$(echo "$data" | cut -f5 -d'|' | tr -s ' ')"'">' >> "$OUT_HTML"
    echo '            <h5>' "$(echo "$data" | cut -f2 -d'|' | tr -s ' ')" '</h5>' >> "$OUT_HTML"
    echo '            <p>' "$(echo "$data" | cut -f3 -d'|' | tr -s ' ')" '</p>' >> "$OUT_HTML"
    echo '          </a>' >> "$OUT_HTML"
    echo '        </div>' >> "$OUT_HTML"
    echo '      </div>' >> "$OUT_HTML"
  fi
done < "$PAGE_DATA"
if [[ closing ]]; then
  echo '  </div>' >> "$OUT_HTML"
fi
cat "$PAGE_END" >> "$OUT_HTML"
